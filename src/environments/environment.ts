// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA4-Fa_BAZ8dOeeE81h_1U3utMs-pSt3Ss",
    authDomain: "testatwo-5ac36.firebaseapp.com",
    projectId: "testatwo-5ac36",
    storageBucket: "testatwo-5ac36.appspot.com",
    messagingSenderId: "279994597246",
    appId: "1:279994597246:web:af0d98aaca3e516f01fd2f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
