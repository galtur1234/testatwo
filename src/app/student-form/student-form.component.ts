import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { PredictionService } from '../prediction.service';
import { StudentsService } from '../students.service';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  @Input() name:string;
  @Input() mathematics:number;
  @Input() psychometric:number;
  @Input() payment:boolean;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>()
  @Output() closeEdit = new EventEmitter<null>()
  rowToSave:boolean = false; 
  result:string;
  email:string;
  timestamp:number =Date.now();


  isErrorMat:boolean = false;
  isErrorpsy:boolean = false;


  constructor(public authService:AuthService, private router:Router, private predictionService:PredictionService, private studentsService:StudentsService ) { }



  updateParent(){
    let student:Student = {id:this.id, name:this.name, mathematics:this.mathematics, psychometric:this.psychometric, payment:this.payment, email:this.email, timestamp:this.timestamp};
    if(this.psychometric < 0 || this.psychometric > 800){
      this.isErrorpsy = true;
    }
    else if(this.mathematics < 0 || this.mathematics > 100){
      this.isErrorMat = true;
    }
    else{
    this.update.emit(student);
    if(this.formType == "Add Student"){
      this.name = null;
      this.mathematics = null;
      this.psychometric = null;
      this.payment = false;
    }
   }
  }

  predict(){
    this.isErrorMat = false;
    this.isErrorpsy = false;
    if(this.psychometric < 0 || this.psychometric > 800){
      this.isErrorpsy = true;
    }
    else if(this.mathematics < 0 || this.mathematics > 100){
      this.isErrorMat = true;
    }
    else{
    this.predictionService.predict(this.mathematics, this.psychometric).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Does not fall off';
        } else {
          var result = 'Falls out'
        }
        this.result = result;
        console.log(result);
        this.rowToSave = true;
      }
    );   
    }
      }


      cancel(){
        this.rowToSave = false;
        this.name = null;
        this.mathematics = null;
        this.psychometric = null;
        this.payment = null;
        this.result = null;
      }

      add(){
        this.studentsService.addStudent(this.name, this.mathematics, this.psychometric, this.payment, this.result, this.email, this.timestamp);
        this.router.navigate(['/students']);
      }
      ngOnInit(): void {
        this.authService.user.subscribe(
          user => {
            this.email = user.email;
          }
        )
      }
}
