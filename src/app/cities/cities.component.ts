import { Component, OnInit } from '@angular/core';
import { CitiesService } from '../cities.service';
import { WeatherService } from '../weather.service';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { ActivatedRoute, Router } from '@angular/router';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  temperature:number; 
  humidity:number;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  panelOpenStat = false;
  ourCities = [{name:'London'},{name:'Paris'},{name:'Tel Aviv'},{name:'Jerusalem'},{name:'Berlin'},{name:'Rome'},{name:'Dubai'},{name:'Athens'}];
  city:string; 

  constructor(private router:Router, private citiesService:CitiesService, private weatherService:WeatherService, private route:ActivatedRoute, private predictionService: PredictionService) { }

  public getCities(){
    return this.ourCities;
  }

  public getData(cityName:string) {
    this.weatherData$ = this.weatherService.searchWeatherData(cityName);
  }
  

  ngOnInit(): void {
    this.ourCities = this.getCities();
  }

}
