import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Weather } from './interfaces/weather';
import { WeatherData } from './interfaces/weather-data';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "2bb90e24be3e5171b9f279e36035561e";
  private IMP = "units=metric";
  
  private transformWeatherData(data:WeatherData):Weather{
    return{
      name:data.name,
      temperature:data.main.temp,
      humidity:data.main.humidity,
    }
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server error');
  }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherData>(`${this.URL}${cityName}&appid=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

  constructor(private http:HttpClient) { }
}

